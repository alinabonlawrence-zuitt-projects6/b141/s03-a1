1.) 
INSERT INTO users (email, password, datetime_created)
VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00"),
("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00"),
("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00"),
("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00"),
("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00"); 

SELECT * FROM users;

+----+-------------------------+-----------+---------------------+
| id | email                   | password  | datetime_created    |
+----+-------------------------+-----------+---------------------+
|  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
|  2 | juandelacruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
|  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
|  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
|  5 | johndoe@gmail.com       | passwordE | 2021-01-01 05:00:00 |
+----+-------------------------+-----------+---------------------+

2.)
INSERT INTO posts (author_id, title, content, datetime_posted)
VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00"),
(1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00"),
(2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00"),
(4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

SELECT * FROM posts;

+----+-----------+-------------+-----------------------+---------------------+
| id | author_id | title       | content               | datetime_posted     |
+----+-----------+-------------+-----------------------+---------------------+
|  1 |         1 | First Code  | Hello World!          | 2021-01-02 01:00:00 |
|  2 |         1 | Second Code | Hello Earth!          | 2021-01-02 02:00:00 |
|  3 |         2 | Third Code  | Welcome to Mars!      | 2021-01-02 03:00:00 |
|  4 |         4 | Fourth Code | Bye bye solar system! | 2021-01-02 04:00:00 |
+----+-----------+-------------+-----------------------+---------------------+

3.)
SELECT * FROM posts WHERE author_id = 1;

+----+-----------+-------------+--------------+---------------------+
| id | author_id | title       | content      | datetime_posted     |
+----+-----------+-------------+--------------+---------------------+
|  1 |         1 | First Code  | Hello World! | 2021-01-02 01:00:00 |
|  2 |         1 | Second Code | Hello Earth! | 2021-01-02 02:00:00 |
+----+-----------+-------------+--------------+---------------------+

4.)
SELECT email, datetime_created FROM users;

+-------------------------+---------------------+
| email                   | datetime_created    |
+-------------------------+---------------------+
| johnsmith@gmail.com     | 2021-01-01 01:00:00 |
| juandelacruz@gmail.com  | 2021-01-01 02:00:00 |
| janesmith@gmail.com     | 2021-01-01 03:00:00 |
| mariadelacruz@gmail.com | 2021-01-01 04:00:00 |
| johndoe@gmail.com       | 2021-01-01 05:00:00 |
+-------------------------+---------------------+

5.)
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

SELECT * FROM posts WHERE id = 2;

+----+-----------+-------------+-----------------------------------+---------------------+
| id | author_id | title       | content                           | datetime_posted     |
+----+-----------+-------------+-----------------------------------+---------------------+
|  2 |         1 | Second Code | Hello to the people of the Earth! | 2021-01-02 02:00:00 |
+----+-----------+-------------+-----------------------------------+---------------------+


6.)
DELETE FROM users WHERE email = "johndoe@gmail.com";

SELECT * FROM users;

+----+-------------------------+-----------+---------------------+
| id | email                   | password  | datetime_created    |
+----+-------------------------+-----------+---------------------+
|  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
|  2 | juandelacruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
|  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
|  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
+----+-------------------------+-----------+---------------------+